#include "Helper.h"
#include <Windows.h>

#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include "shlwapi.h"
#include <filesystem>

#define ZERO 0
#define ONE 1
#define PWD "pwd"
#define CD "cd"
#define CREATE "create"
#define LS "ls"
#define SECRET "secret"
#define TWO_DOTS ".."
#define SLASH "\\"

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

typedef unsigned int (WINAPI* AvVersion)(void);

int main()
{
	Helper help;
	string comd;
	
	vector<string> vec;
	char* path = new char[MAX_PATH];
	GetModuleFileNameA(NULL, path, MAX_PATH);

	string directory = path;
	int slashLastIndex;


	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	HANDLE h;


	while (true)
	{
		cout << ">> ";

		std::getline(cin, comd);
		help.trim(comd);
		vec = help.get_words(comd);

		if (vec[ZERO].compare(PWD) == ZERO) //Checks if the command if pwd
		{
			cout << directory << endl;
		}
		else if (vec[ZERO].compare(CD) == ZERO) //Checks if the command is cd
		{
			if (vec[ONE].compare(TWO_DOTS) == 0) //Checks if the command is cd ..
			{
				slashLastIndex = directory.find_last_of(SLASH);
				if (slashLastIndex != string::npos) //Checking if there is a "\" in the current path
				{
					directory = directory.substr(0, slashLastIndex);
					SetCurrentDirectoryA(directory.c_str());
				}
				else
				{
					cout << "Could not go back. There is no \\ in the current path" << endl;
				}
			}
			/*
			משום מה האפשרות הזאת לא עובדת
			*/
			//else if (PathFileExists((directory + "\\" + vec[ONE]).c_str()))
			//{
				//directory = directory + "\\" + vec[ONE];
				//SetCurrentDirectoryA(directory.c_str());
			//}
			else //It is a regular cd
			{
				SetCurrentDirectoryA(vec[ONE].c_str());
				directory = vec[ONE];
			}
		}
		else if (vec[ZERO].compare(CREATE) == ZERO) //Checking if the command is create
		{
			h = CreateFile(vec[ONE].c_str(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

			if (h)
			{
				std::cout << "CreateFile() succeeded\n";
				CloseHandle(h);
			}
			else
			{
				std::cerr << "CreateFile() failed:" << GetLastError() << "\n";
			}
		}
		else if (vec[ZERO].compare(LS) == ZERO) //Checking if the command is ls
		{
			// Prepare string for use with FindFile functions.  First, copy the
			// string to a buffer, then append '\*' to the directory name.

			StringCchCopy(szDir, MAX_PATH, directory.c_str());
			StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

			// Find the first file in the directory.

			hFind = FindFirstFile(szDir, &ffd);
			// List all the files in the directory with some info about them.

			do
			{
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
				}
				else
				{
					filesize.LowPart = ffd.nFileSizeLow;
					filesize.HighPart = ffd.nFileSizeHigh;
					_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
				}
			} while (FindNextFile(hFind, &ffd) != 0);

			dwError = GetLastError();
		}
		else if (vec[ZERO].compare(SECRET) == ZERO) //Checking if the command is secret
		{
			HMODULE dll = LoadLibraryA("SECRET.dll");
			if (dll != NULL)
			{
				AvVersion func = (AvVersion)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
				if (func != NULL)
				{
					int result = func();
					cout << result << endl;
				}
				else
				{
					cout << "Could not open the function" << endl;
				}
			}
			else
			{
				cout << "Could not open the dll" << endl;
			}
		}
		else
		{
			WinExec(LPSTR(vec[ZERO].c_str()), SW_SHOWNORMAL);
		}
	}

	return 0;
}